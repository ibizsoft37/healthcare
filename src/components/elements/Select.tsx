import * as React from 'react';
import { StatefulSelect } from 'baseui/select';
import { FormControl } from 'baseui/form-control';
import classNames from 'classnames';

const formControlOverrides = {
	Label: { style: { fontSize: '14px', color: 'rgba(26, 57, 101, 1)' } }
};

const singleAgentStatefulSelectOverrides = {
	ControlContainer: {
		style: {
			borderColor: 'rgba(0,0,0,0)',
			borderBottom: '1px solid #ced4da',
			borderBottomRightRadius: '0px',
			borderTopRightRadius: '0px',
			borderLeftRightRadius: '0px',
			borderRightRightRadius: '0px',
			backgroundColor: 'rgba(0,0,0,0)'
		}
	}
};

const multiAgentSatefulSelectOverrides = {
	MultiValue: {
		props: {
			color: '#dc176c',
			kind: 'custom',
			overrides: {
				Root: {
					style: {
						backgroundColor: 'rgba(220, 23, 108, 0.05)',
						color: '#dc176c',
						fontSize: '14px'
					}
				}
			}
		}
	},
	ControlContainer: {
		style: {
			borderColor: 'rgba(0,0,0,0)',
			borderBottom: '1px solid #ced4da',
			borderBottomRightRadius: '0px',
			borderTopRightRadius: '0px',
			borderLeftRightRadius: '0px',
			borderRightRightRadius: '0px',
			backgroundColor: 'rgba(0,0,0,0)'
		}
	}
};

class Select extends React.Component<{
	style?: string;
	label: string;
	isLoading?: boolean;
	options: any[];
	onChange: (e) => void;
	labelKey: string;
	placeholder: string;
	valueKey: string;
	disabled?: boolean;
	multi?: boolean;
}> {
	render() {
		const {
			style,
			label,
			isLoading,
			options,
			onChange,
			labelKey,
			placeholder,
			valueKey,
			disabled,
			multi
		} = this.props;
		const selectClass = classNames(style);
		return (
			<div className={selectClass}>
				<FormControl overrides={formControlOverrides} label={label}>
					<StatefulSelect
						isLaoding={isLoading}
						multi={!!multi}
						disabled={disabled}
						options={options}
						labelKey={labelKey}
						valueKey={valueKey}
						placeholder={placeholder}
						onChange={onChange}
						overrides={
							multi
								? multiAgentSatefulSelectOverrides
								: singleAgentStatefulSelectOverrides
						}
					/>
				</FormControl>
			</div>
		);
	}
}

export default Select;
